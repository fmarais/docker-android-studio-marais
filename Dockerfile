# NOTE
# Items marked with ## are comments
# items marked with # are code snippets that can be enabled

FROM ubuntu:12.04
MAINTAINER Francois Marais <fm.marais@gmail.com>
##
##
##
##
## 	--------------------- Before you build
## 	1. (MANDATORY) Download the android studio zip 
## 	https://developer.android.com/sdk/index.html#Other
## 	copy the zip to to the Dockerfile directory and rename to 
##  'android-studio.zip'

## 	2. (MANDATORY) Download Java JDK tar.gz 
## 	http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
## 	copy the java jdk tar.gz to the Dockerfile directory and rename to 
##  'jdk.tar.gz'

## 	3. (OPTIONAL, ENABLE IF YOU WANT TO USE EXISTING SDK DIR) 
## 	copy your android sdk directory to the Dockerfile directory and rename to 
##  'android-studio-sdk' 
RUN mkdir -p /home/developer/android-studio-sdk
COPY android-studio-sdk /home/developer/android-studio-sdk

## 4. (OPTIONAL, FOR GENYMOTION) 
## Install virtualbox and genymotion on the host machine
## Install geymotion under $HOME/programs/genymotion on the host machine
## Use the genymotion start method
##
##
##
##
## --------------------- Build and Run
## 	Check that you have your 
##  1. android-studio.zip 
##  2. jdk.tar.gz
##  3. optional android-studio-sdk folder
##	Build with the following command
#docker build -t android-studio/marais .

##	Start new container, 1st time (no container), NOT using GENYMOTION
#xhost + && \
#sudo docker run -it \
#-e DISPLAY \
#--privileged -v /dev/bus/usb:/dev/bus/usb \
#-v $HOME/Desktop:/home/developer/Desktop \
#-v $HOME/projects/android:/home/developer/projects \
#--net=host \
#--name android-studio \
#android-studio/marais \
#/home/developer/android-studio/bin/studio.sh

##	Start new container, 1st time (no container), USING GENYMOTION
#xhost + && \
#sudo docker run -it \
#-e DISPLAY \
#--privileged -v /dev/bus/usb:/dev/bus/usb \
#-v $HOME/Desktop:/home/developer/Desktop \
#-v $HOME/projects/android:/home/developer/projects \
#-v $HOME/programs/genymotion:/home/developer/genymotion \
#-v $HOME/.Genymobile:$HOME/.Genymobile \
#--net=host \
#-p 8600-8699:8600-8699 \
#--name android-studio \
#android-studio/marais \
#/home/developer/android-studio/bin/studio.sh

## Start existing container, 2nd time
#xhost + && sudo docker start android-studio
##
##
##
##
## --------------------- Init and tools
RUN apt-get update
RUN apt-get install -y \
nano \
unzip \
bzip2 \
git \
libxtst6 \
libxtst6:i386 \
lib32stdc++6 \
libxrender-dev \
libxi6 \
libgconf-2-4

## Required for android studio gradle build process
RUN apt-get install -y \
libncurses5:i386 \
libstdc++6:i386 \
zlib1g:i386

## Git flow
RUN apt-get install -y \
git-flow

## Bash-it for git shell
RUN git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it && \
~/.bash_it/install.sh
##
##
##
##
## --------------------- Java Installation (/usr/lib/jvm/java-oracle-jdk)
## Download you desired java JDK, http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
RUN mkdir -p /usr/lib/jvm/java-oracle-jdk
COPY jdk.tar.gz /tmp/jdk.tar.gz
RUN tar zxf /tmp/jdk.tar.gz -C /usr/lib/jvm/java-oracle-jdk --strip-components=1
RUN rm /tmp/jdk.tar.gz
ENV JAVA_HOME /usr/lib/jvm/java-oracle-jdk
##
##
##
##
## --------------------- Android studio installation (/home/developer/android-studio)
## Download android studio, https://developer.android.com/sdk/index.html#Other
## Project files from host machine in $HOME/projects/android
RUN mkdir -p /home/developer/projects
COPY android-studio.zip /tmp/android-studio.zip
RUN unzip -d /home/developer /tmp/android-studio.zip
RUN rm /tmp/android-studio.zip

## Set up USB device debugging
RUN mkdir -p /etc/udev/rules.d
COPY 51-android.rules /etc/udev/rules.d/51-android.rules
RUN chmod a+r /etc/udev/rules.d/51-android.rules

## Add env for studio
ENV PATH $PATH:/home/developer/android-studio-sdk/tools
ENV PATH $PATH:/home/developer/android-studio-sdk/platform-tools
##
##
##
##
## --------------------- Other
## Add ssh
COPY ssh.zip /tmp/ssh.zip
RUN unzip -d /root/.ssh /tmp/ssh.zip
RUN rm /tmp/ssh.zip

## Git config, change where required
RUN git config --global user.name "marais"
RUN git config --global user.email "fm.marais@gmail.com"
RUN git config --global color.diff auto

## Add some aliases for git and git flow
RUN echo "# git" >> /root/.bashrc
RUN echo "alias gdf='git diff'" >> /root/.bashrc
RUN echo "alias gst='git status'" >> /root/.bashrc
RUN echo "alias gad='git add -A'" >> /root/.bashrc
RUN echo "alias gba='git branch -a'" >> /root/.bashrc
RUN echo "alias gcm='git commit -m'" >> /root/.bashrc
RUN echo "alias rake='bundle exec rake'" >> /root/.bashrc
RUN echo "alias gco='git checkout'" >> /root/.bashrc
RUN echo "alias gpl='git pull origin'" >> /root/.bashrc
RUN echo "alias gps='git push origin'" >> /root/.bashrc
RUN echo "alias gf='git fetch -p'" >> /root/.bashrc
RUN echo "alias gu='git fetch -p && git checkout master && git pull origin master && git checkout develop && git pull origin develop && git branc branch -a'" >> /root/.bashrc
RUN echo "" >> /root/.bashrc
RUN echo "# git flow" >> /root/.bashrc
RUN echo "alias fs='git flow feature start'" >> /root/.bashrc
RUN echo "alias ff='git flow feature finish -F'" >> /root/.bashrc
RUN echo "alias hs='git flow hotfix start'" >> /root/.bashrc
RUN echo "alias hf='git flow hotfix finish'" >> /root/.bashrc
RUN echo "alias rs='git flow release start'" >> /root/.bashrc
RUN echo "alias rf='git flow release finish'" >> /root/.bashrc