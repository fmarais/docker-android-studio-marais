# Docker Environment for Android Studio

## Installed items
- Ubuntu 12.04
- Android studio of choice
- Java JDK of choice (7 or 8)
- Optional android-sdk import from previous installation
- Optional genymotion installed on host machine

```
- Supports adb debugging
- Host machine contains the project files and are shared with docker container
- Paths are added so commands like 'adb' will be available
- Android studio and android-sdk are seperate, easy to swap out if required
- Bash-it shell installed for nice git interface
```

## File/Folder list
- 51-android.rules 		(mandatory) (file)
- android-studio.zip 	(mandatory) (file)
- Dockerfile 			(mandatory) (file)
- jdk.tar.gz 			(mandatory) (file)
- README.md 			(mandatory) (file)
- ssh.zip 				(optional) 	(file)
- android-studio-sdk 	(optional) 	(folder)

**Build and run instructions are in the Dockerfile.**
